package com.example.demo.controller;

import org.springframework.web.bind.annotation.*;

import java.util.List;

import javax.validation.Valid;

import com.example.demo.dto.ToDoResponse;
import com.example.demo.dto.ToDoSaveRequest;
import com.example.demo.exception.ToDoNotFoundException;
import com.example.demo.service.ToDoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestController
public class ToDoController {

    @Autowired
    ToDoService toDoService;

    @ExceptionHandler({ToDoNotFoundException.class})
    public String handleException(Exception ex) {
        return ex.getMessage();
    }

    @GetMapping("/todos")
    @Valid List<ToDoResponse> getAll() {
        return toDoService.getAll();
    }

    @PostMapping("/todos")
    @Valid ToDoResponse save(@Valid @RequestBody ToDoSaveRequest todoSaveRequest) throws ToDoNotFoundException {
        return toDoService.upsert(todoSaveRequest);
    }

    @PutMapping("/todos/{id}/complete")
    @Valid ToDoResponse save(@PathVariable Long id) throws ToDoNotFoundException {
        return toDoService.completeToDo(id);
    }

    @GetMapping("/todos/{id}")
    @Valid ToDoResponse getOne(@PathVariable Long id) throws ToDoNotFoundException {
        return toDoService.getOne(id);
    }

    @GetMapping("/todos/{phrase}")
    @Valid List<ToDoResponse> getAllWithPhrase(@PathVariable String phrase) {
        return toDoService.findAllWithPhrase(phrase);
    }

    @DeleteMapping("/todos/{id}")
    void delete(@PathVariable Long id) {
        toDoService.deleteOne(id);
    }

    @DeleteMapping("/todos")
    void deleteAll() {
        toDoService.deleteAll();
    }

    @GetMapping("/todos/{id}/pdf")
    @Valid ToDoResponse saveAsPDF(@PathVariable Long id) throws ToDoNotFoundException {
        return toDoService.saveAsPDF(id);
    }
}