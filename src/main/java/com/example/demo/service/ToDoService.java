package com.example.demo.service;

import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.example.demo.dto.ToDoResponse;
import com.example.demo.dto.ToDoSaveRequest;
import com.example.demo.dto.mapper.ToDoEntityToResponseMapper;
import com.example.demo.exception.ToDoNotFoundException;
import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;

@Service
public class ToDoService {
	
	private ToDoRepository toDoRepository;

	public ToDoService(ToDoRepository toDoRepository) {
		this.toDoRepository = toDoRepository;
	}

	public List<ToDoResponse> getAll() {
		return toDoRepository.findAll().stream()
			.map(ToDoEntityToResponseMapper::map)
			.collect(Collectors.toList()); 
	}

	public ToDoResponse upsert(ToDoSaveRequest toDoDTO) throws ToDoNotFoundException {
		ToDoEntity todo;
		//update if it has id or create if it hasn't
		if (toDoDTO.id == null) {
			todo = new ToDoEntity(toDoDTO.text);
		} else {
			todo = toDoRepository.findById(toDoDTO.id).orElseThrow(() -> new ToDoNotFoundException(toDoDTO.id));
			todo.setText(toDoDTO.text);
		}
		return ToDoEntityToResponseMapper.map(toDoRepository.save(todo));
	}

	public ToDoResponse completeToDo(Long id) throws ToDoNotFoundException {
		ToDoEntity todo = toDoRepository.findById(id).orElseThrow(() -> new ToDoNotFoundException(id));
		todo.completeNow();
		return ToDoEntityToResponseMapper.map(toDoRepository.save(todo));
	}

	public ToDoResponse getOne(Long id) throws ToDoNotFoundException {
		return  ToDoEntityToResponseMapper.map(
			toDoRepository.findById(id).orElseThrow(() -> new ToDoNotFoundException(id))
		);
	}

	public void deleteOne(Long id) {
		toDoRepository.deleteById(id);
	}

	public List<ToDoResponse> findAllWithPhrase(String phrase) {
		List<ToDoResponse> toDoResponses = toDoRepository.findAll().stream()
				.map(ToDoEntityToResponseMapper::map)
				.collect(Collectors.toList());
		toDoResponses.removeIf(toDo -> !toDo.getText().toLowerCase().contains(phrase.toLowerCase()));
		return toDoResponses;
	}

	public ToDoResponse saveAsPDF(Long id) throws ToDoNotFoundException {
		ToDoEntity todo = toDoRepository.findById(id).orElseThrow(() -> new ToDoNotFoundException(id));
		ToDoResponse response = ToDoEntityToResponseMapper.map(todo);
		Document document = new Document();
		try {
			Files.createDirectories(Paths.get("files"));
		} catch (IOException e) {
			throw new RuntimeException("Can`t create root directory!");
		}

		String path = "files" + "/" + response.getId() + ".pdf";

		try {
			PdfWriter.getInstance(document, new FileOutputStream(path));
			document.open();
			Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
			Chunk chunk = new Chunk(response.getText(), font);

			document.add(chunk);
			document.close();
		} catch (DocumentException | FileNotFoundException e) {
			e.printStackTrace();
		}

		String url = "http://localhost:" + "8082" + "/" + path;

		response.setText(url);
		return response;
	}

	public void deleteAll() {
		toDoRepository.deleteAll();
	}
}
