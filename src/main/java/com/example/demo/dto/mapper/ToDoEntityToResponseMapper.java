package com.example.demo.dto.mapper;

import com.example.demo.dto.ToDoResponse;
import com.example.demo.model.ToDoEntity;

public class ToDoEntityToResponseMapper {
	public static ToDoResponse map(ToDoEntity todoEntity) {
		if (todoEntity == null)
			return null;
		var result = new ToDoResponse();
		result.setId(todoEntity.getId());
		result.setText(todoEntity.getText());
		result.setCompletedAt(todoEntity.getCompletedAt());
		return result;
	}
}