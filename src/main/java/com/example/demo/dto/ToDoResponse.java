package com.example.demo.dto;

import lombok.Data;

import java.time.ZonedDateTime;

import javax.validation.constraints.NotNull;

@Data
public class ToDoResponse {
	@NotNull
	private Long id;

	@NotNull
	private String text;

	private ZonedDateTime completedAt;
}